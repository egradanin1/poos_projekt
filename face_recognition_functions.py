from PIL import Image
import face_recognition
import cv2
import csv
import numpy as np
import glob
import os


def switch_between_rgb_and_bgr(image):
    b, g, r = cv2.split(image)
    return cv2.merge([r, g, b])


def create_csv_for_image_face_location(image, image_name):
    face_locations = face_recognition.face_locations(image)
    if len(face_locations) > 0:
        top, right, bottom, left = face_locations[0]
        with open('face_locations.csv', 'a', newline='') as csvfile:
            writter = csv.writer(csvfile, delimiter=',',
                                 quotechar='|', quoting=csv.QUOTE_MINIMAL)
            writter.writerow([image_name, top, right, bottom, left])


def read_image_face_location_from_csv(image_name):
    face_location = None
    with open('face_locations.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in reader:
            if row[0] == image_name:
                face_location = row[1:]
    return face_location


def create_and_store_image_mask(image, face_location, image_name):
    if face_location is not None:
        face_location = [int(x) for x in face_location]
        top, right, bottom, left = face_location
        mask = np.zeros(image.shape, np.uint8)
        mask[top:bottom, left:right] = image[top:bottom, left:right]
        cv2.imwrite('masks/mask_' + os.path.basename(image_name),
                    switch_between_rgb_and_bgr(mask))


def median_filter(image, image_name):
    cv2.imwrite('sumredukcija/median_' + image_name,
                cv2.medianBlur(switch_between_rgb_and_bgr(image), 3))


def unsharp_masking(image_name, k):
    image = cv2.imread('sumredukcija/median_' + image_name, 3)
    gaussian_3 = cv2.GaussianBlur(image, (9, 9), 10.0)
    g_image = image - gaussian_3
    unsharp_image = cv2.addWeighted(image, 1, g_image, -k, 0, image)
    cv2.imwrite('maskiranjeneostrina/gaussian_' + image_name, unsharp_image)


# Contrast Limited Adaptive Histogram Equalization

def increase_contrast(image, image_name):
    image = switch_between_rgb_and_bgr(image)
    lab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    l, a, b = cv2.split(lab)
    clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8, 8))
    cl = clahe.apply(l)
    limg = cv2.merge((cl, a, b))
    result = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)
    cv2.imwrite('contrast_filter/contrast_' + image_name, result)


# thresholding
def increase_brightness(image, image_name):
    image = switch_between_rgb_and_bgr(image)
    cutoff_val = 70  # everything above this is set to set_color
    set_color = 245
    ret, thresh_img = cv2.threshold(
        image, cutoff_val, set_color, cv2.THRESH_BINARY)
    cv2.imwrite('brightness_filter/brightness_' + image_name, thresh_img)


def equalize_histogram(image, image_name):
    image = switch_between_rgb_and_bgr(image)
    img_yuv = cv2.cvtColor(image, cv2.COLOR_BGR2YUV)
    img_yuv[:, :, 0] = cv2.equalizeHist(img_yuv[:, :, 0])
    img_output = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)
    cv2.imwrite('histogram_filter/histogram_' + image_name, img_output)


image_list_train = []
for filename in glob.glob('dataset/train/*'):
    image = cv2.imread(filename, 3)
    image = switch_between_rgb_and_bgr(image)
    image_list_train.append(tuple((filename, image)))


image_list_test = []
for filename in glob.glob('dataset/test/*'):
    image = cv2.imread(filename, 3)
    image = switch_between_rgb_and_bgr(image)
    image_list_test.append(tuple((filename, image)))


def create_and_store_image_mask_for_all_images():
    for image in image_list_train:
        if read_image_face_location_from_csv(image[0]) is None:
            create_csv_for_image_face_location(image[1], image[0])
        create_and_store_image_mask(
            image[1], read_image_face_location_from_csv(image[0]), image[0])
    for image in image_list_test:
        if read_image_face_location_from_csv(image[0]) is None:
            create_csv_for_image_face_location(image[1], image[0])
        create_and_store_image_mask(
            image[1], read_image_face_location_from_csv(image[0]), image[0])


def median_filter_for_all_images():
    for image in image_list_train:
        median_filter(image[1], os.path.basename(image[0]))
    for image in image_list_test:
        median_filter(image[1], os.path.basename(image[0]))


def unsharp_masking_for_all_images(k):
    for image in image_list_train:
        unsharp_masking(os.path.basename(image[0]), k)
    for image in image_list_test:
        unsharp_masking(os.path.basename(image[0]), k)


def increase_contrast_for_all_images():
    for image in image_list_train:
        increase_contrast(image[1], os.path.basename(image[0]))
    for image in image_list_test:
        increase_contrast(image[1], os.path.basename(image[0]))


def increase_brightness_for_all_images():
    for image in image_list_train:
        increase_brightness(image[1], os.path.basename(image[0]))
    for image in image_list_test:
        increase_brightness(image[1], os.path.basename(image[0]))


def equalize_histogram_for_all_images():
    for image in image_list_train:
        equalize_histogram(image[1], os.path.basename(image[0]))
    for image in image_list_test:
        equalize_histogram(image[1], os.path.basename(image[0]))


# create_and_store_image_mask_for_all_images()
# median_filter_for_all_images()
# unsharp_masking_for_all_images(0.1)
# increase_contrast_for_all_images()
# increase_brightness_for_all_images()
# equalize_histogram_for_all_images()
