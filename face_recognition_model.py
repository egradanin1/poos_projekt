import math
from sklearn import neighbors
import os
import os.path
import re
import pickle
import face_recognition
import shutil
from random import randrange
import copy
import cv2
import dlib
import numpy as np
import face_recognition_models
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from PIL import Image, ImageDraw
import glob

frontal_face_detector = dlib.get_frontal_face_detector()
pose_predictor_five_point_model = face_recognition_models.pose_predictor_five_point_model_location()
shape_predictor = dlib.shape_predictor(pose_predictor_five_point_model)
face_recognition_model = face_recognition_models.face_recognition_model_location()
face_encoder = dlib.face_recognition_model_v1(face_recognition_model)


def get_images_from_folder(folder):
    return [os.path.join(folder, f) for f in os.listdir(folder) if re.match(r'.*\.(jpg|jpeg|png)', f, flags=re.I)]


def face_areas(image, face_locations=None):
    if face_locations is None:
        face_locations = frontal_face_detector(image, 1)
    else:
        face_locations = [dlib.rectangle(face_location[3], face_location[0], face_location[1], face_location[2]) for face_location in face_locations]

    pose_predictor = shape_predictor

    return [pose_predictor(image, face_location) for face_location in face_locations]


def encode_face(image, face_locations=None):
    faces = face_areas(image, face_locations)
    return [np.array(face_encoder.compute_face_descriptor(image, face, 1)) for face in faces]


def train(n_neighbors=None, folder_path="dataset/train", knn_algo='ball_tree'):
    X = []
    y = []

    for class_dir in os.listdir(folder_path):
        if not os.path.isdir(os.path.join(folder_path, class_dir)):
            continue
        for img_path in get_images_from_folder(os.path.join(folder_path, class_dir)):
            image = face_recognition.load_image_file(img_path)
            face_bounding_boxes = face_recognition.face_locations(image)
            if len(face_bounding_boxes) != 0:
                X.append(encode_face(image, face_bounding_boxes)[0])
                y.append(class_dir)
    if n_neighbors is None:
        n_neighbors = int(round(math.sqrt(len(X))))

    knn_clf = neighbors.KNeighborsClassifier(n_neighbors=n_neighbors, algorithm=knn_algo, weights='distance')
    knn_clf.fit(X, y)

    with open("trained_knn_model.clf", 'wb') as f:
        pickle.dump(knn_clf, f)


def predict(filename, distance_threshold=0.6):

    with open("trained_knn_model.clf", 'rb') as f:
        knn_clf = pickle.load(f)

    image = face_recognition.load_image_file(filename)
    face_locations = face_recognition.face_locations(image)

    if len(face_locations) == 0:
        return []

    faces_encodings = encode_face(image, face_locations)

    closest_distances = knn_clf.kneighbors(faces_encodings, n_neighbors=1)
    are_matches = [closest_distances[0][i][0] <= distance_threshold for i in range(len(face_locations))]

    return [(pred, loc) if rec else ("unknown", loc) for pred, loc, rec in zip(knn_clf.predict(faces_encodings), face_locations, are_matches)]


def get_images_names():
    all_images = {}
    for class_dir in os.listdir("new_dataset"):
        if not os.path.isdir(os.path.join("new_dataset", class_dir)):
            continue
        images = []
        for img_path in get_images_from_folder(os.path.join("new_dataset", class_dir)):
            images.append(img_path)
        all_images[class_dir] = images
    return all_images


def switch_between_rgb_and_bgr(image):
    b, g, r = cv2.split(image)
    return cv2.merge([r, g, b])


def add_enhancement(image_path):
    image = switch_between_rgb_and_bgr(cv2.imread(image_path, 3))
    face_locations = face_recognition.face_locations(image)
    if len(face_locations) != 0:
        top, right, bottom, left = face_locations[0]
        mask = image[top:bottom, left:right]
        img_yuv = cv2.cvtColor(mask, cv2.COLOR_BGR2YUV)
        img_yuv[:, :, 0] = cv2.equalizeHist(img_yuv[:, :, 0])
        img_output = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)
        image[top:bottom, left:right] = cv2.medianBlur(img_output, 3)
    return switch_between_rgb_and_bgr(image)


def randomly_distributed_images(images):
    number_of_images = 0
    images_copy = copy.deepcopy(images)
    for person_images in images_copy:
        number_of_images = number_of_images + len(images_copy[person_images])
    number_of_validation_images = int(round(number_of_images * 0.1)) // 3
    number_of_test_and_train_images = (number_of_images - number_of_validation_images) // 6
    for person_images in images_copy:
        for folder_name in os.listdir("dataset"):
            full_path = os.path.join("dataset", folder_name, person_images)
            if os.path.isdir(full_path):
                shutil.rmtree(full_path)
            os.makedirs(full_path)
            for i in range(0, number_of_test_and_train_images if folder_name == "test" or folder_name == "train" else number_of_validation_images):
                if len(images_copy[person_images]) != 0:
                    random_index = randrange(len(images_copy[person_images]))
                    image_path = images_copy[person_images][random_index]
                    del images_copy[person_images][random_index]
                    cv2.imwrite(full_path + "/" + os.path.basename(image_path), add_enhancement(image_path))


def calculate_perfomance_factors(true, predicted):
    print(len(true), true)
    print("\n")
    print(len(predicted), predicted)
    print("Tacnost cijelog modela: " + str(accuracy_score(true, predicted)))
    perfomance_matrix = confusion_matrix(true, predicted, labels=["alcapone", "johnnydepp", "rowanatkinson"])
    tp = [perfomance_matrix[0][0], perfomance_matrix[1][1], perfomance_matrix[2][2]]
    tn = [perfomance_matrix[1][1] + perfomance_matrix[2][2], perfomance_matrix[0][0] + perfomance_matrix[2][2], perfomance_matrix[0][0] + perfomance_matrix[1][1]]
    fp = [perfomance_matrix[1][0] + perfomance_matrix[2][0], perfomance_matrix[0][1] + perfomance_matrix[2][1], perfomance_matrix[0][2] + perfomance_matrix[1][2]]
    fn = [perfomance_matrix[0][1] + perfomance_matrix[0][2], perfomance_matrix[1][0] + perfomance_matrix[1][2], perfomance_matrix[2][0] + perfomance_matrix[2][1]]
    ep = [tp[x] + fn[x] for x in range(len(tp))]
    en = [fp[x] + tn[x] for x in range(len(fp))]
    classes = ["alcapone", "johnnydepp", "rowanatkinson"]
    for i in range(0, 3):
        sens = tp[i] / ep[i]
        spec = tn[i] / en[i]
        acc = (tp[i] + tn[i]) / (ep[i] + en[i])
        print("Faktori perfomansi za klasu: " + str(classes[i]))
        print("Tacnost je: " + str(acc) + "\n" + "Senzitivnost je: " + str(sens) + "\n" + "Specificnost je: " + str(spec) + "\n")


def face_name_label_image(img_path, predictions):
    label_on_image = Image.open(img_path).convert("RGB")
    draw = ImageDraw.Draw(label_on_image)

    for name, (top, right, bottom, left) in predictions:
        draw.rectangle(((left, top), (right, bottom)), outline=(0, 0, 255))
        name = name.encode("UTF-8")
        text_width, text_height = draw.textsize(name)
        draw.rectangle(((left, bottom - text_height - 10), (right, bottom)), fill=(0, 0, 255), outline=(0, 0, 255))
        draw.text((left + 6, bottom - text_height - 5), name, fill=(255, 255, 255, 255))

    del draw
    label_on_image.save("labelisane_slike/" + "label_name_" + os.path.basename(img_path))


def clustering(folder_path):
    descriptors = []
    images = []

    for class_dir in os.listdir(folder_path):
        if not os.path.isdir(os.path.join(folder_path, class_dir)):
            continue
        for f in glob.glob("clustered/" + class_dir + "/*"):
            os.remove(f)
        for filename in get_images_from_folder(os.path.join(folder_path, class_dir)):
            img = dlib.load_rgb_image(filename)
            dets = frontal_face_detector(img, 1)

            for k, d in enumerate(dets):
                shape = shape_predictor(img, d)
                face_descriptor = face_encoder.compute_face_descriptor(img, shape)
                descriptors.append(face_descriptor)
                images.append((img, shape))
        labels = dlib.chinese_whispers_clustering(descriptors, 0.5)
        num_classes = len(set(labels))
        biggest_class = None
        biggest_class_length = 0
        for i in range(0, num_classes):
            class_length = len([label for label in labels if label == i])
            if class_length > biggest_class_length:
                biggest_class_length = class_length
                biggest_class = i
        indices = []
        for i, label in enumerate(labels):
            if label == biggest_class:
                indices.append(i)
        for i, index in enumerate(indices):
            img, shape = images[index]
            file_path = os.path.join("clustered/" + class_dir, "face_" + str(i))
            dlib.save_face_chip(img, shape, file_path, size=150, padding=0.25)


def testing(folder_path):
    predicted = []
    true = []
    true_dict = {}
    pred_dict = {}
    for f in glob.glob("labelisane_slike/*"):
        os.remove(f)
    for class_dir in os.listdir(folder_path):
        if not os.path.isdir(os.path.join(folder_path, class_dir)):
            continue
        for img_path in get_images_from_folder(os.path.join(folder_path, class_dir)):
            predictions = predict(img_path)  # predikcija na osnovu kreiranog modela
            true.append(img_path.split("/")[2])
            true_dict[img_path] = img_path.split("/")[2]
            if len(predictions) == 0:
                predicted.append("unknown")
                pred_dict[img_path] = "unknown"

            else:
                face_name_label_image(img_path, predictions)
                for name, (top, right, bottom, left) in predictions:
                    predicted.append(name)
                    pred_dict[img_path] = name

                    # print("- Found {} at ({}, {}) on image {}".format(name, left, top, img_path))
    print(true_dict)
    print("\n")
    print(pred_dict)
    print("\n")
    calculate_perfomance_factors(true, predicted)


def main(folder_path):
    # primjena poboljsavanja nad svim slikama i nasumicno razvrstavanje u foldere
    # randomly_distributed_images(get_images_names())

    # kreiranje i treniranje modela uz kreiranje deskriptora, zakomentirati ako se zeli koristiti exportovani model

    # train(n_neighbors=2)

    # pokusaj poboljsanja uz povecavanje broja susjeda
    # train(n_neighbors=3)

    # pokusaj poboljsanja uz promjenu algoritma kod knn modela
    # train(n_neighbors=2, knn_algo='kd_tree')

    # pokusaj poboljsanja uz promjenu algoritma kod knn modela i povecavanje broja susjeda
    # train(n_neighbors=3, knn_algo='auto')

    testing("dataset/test")
    # clustering(folder_path)


main("dataset/train")
